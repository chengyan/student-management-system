package com.chy.student.mapper;


import com.chy.student.domain.Student;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface StudentMapper {


    List<Student> findAll();
}
