package com.chy.student.domain;


import lombok.Data;

@Data
public class Score {

    private int id;//成绩编号
    private int studentId;
    private int courseId;
    private String courseName;
    private int score;

}
