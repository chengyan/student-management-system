package com.chy.student.domain;

import lombok.Data;

@Data
public class Course {
    private int courseId;
    private String courseName;
    private int teacherId;
    private int hour;//课时
}
