package com.chy.student.controller;


import com.chy.student.domain.Teacher;
import com.chy.student.service.TeacherService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class teacherController {

    @Autowired
    TeacherService teacherService;

    //新增教师
    @RequestMapping(value = "/addTeacher", method = RequestMethod.GET)
    public int addTeacher(@Param("name") String name) {


        Teacher teacher = new Teacher();
        teacher.setName(name);
        return teacherService.addTeacher(teacher);
    }


    //删除教师
    @RequestMapping(value = "/deleteTeacher", method = RequestMethod.GET)
    public String deleteTeacher(int id) {

        return teacherService.deleteTeacher(id);
    }


    //更新教师信息
    @RequestMapping(value = "/updateTeacher", method = RequestMethod.GET)
    public String updateTeacher(@Param("id")int id,@Param("name")String name) {

        return teacherService.updateTeacher(id,name);
    }



    //查询教师信息
    @RequestMapping(value = "/findAllTeacher", method = RequestMethod.GET)
    public List<Teacher> findAllTeacher() {

        return teacherService.findAllTeacher();
    }


    //根据姓名模糊查询
    @RequestMapping(value = "/findTeacherByName", method = RequestMethod.GET)
    public List<Teacher> findTeacherByName(@Param("name") String name){


        return teacherService.findTeacherByName(name);
    }
}
