package com.chy.student.domain;


import lombok.Data;

@Data
public class Student {
    private int id;
    private String name;
    private String email;
    private int phone;
    private String branch;
}
