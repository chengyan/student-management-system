package com.chy.student.service;

import com.chy.student.domain.Score;
import com.chy.student.mapper.GradeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GradeService {

    @Autowired
    GradeMapper gradeMapper;
    public List<Score> findAllCourseGrade() {

       return gradeMapper.findAllCourseGrade();
    }
}
