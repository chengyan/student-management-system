package com.chy.student.mapper;

import com.chy.student.domain.Teacher;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface TeacherMapper {


    void addTeacher(Teacher teacher);

    List<Teacher> findAllTeacher();

    List<Teacher> findTeacherByName(@Param("name") String name);

    void deleteTeacher(@Param("id")int id);

    void updateTeacher(@Param("id")int id ,@Param("name") String name);
}
