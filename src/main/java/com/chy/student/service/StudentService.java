package com.chy.student.service;

import com.chy.student.domain.Student;
import com.chy.student.mapper.StudentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {


    @Autowired
    StudentMapper studentMapper;

    public List<Student> findAll() {

        return studentMapper.findAll();
    }
}
