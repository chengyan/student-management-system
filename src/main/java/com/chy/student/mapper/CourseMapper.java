package com.chy.student.mapper;


import com.chy.student.domain.Course;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


@Mapper
public interface CourseMapper {

    List<Course> findAllCourse();
}
