package com.chy.student.controller;

import com.chy.student.domain.Student;
import com.chy.student.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StudentController {

    @Autowired
    StudentService studentService;

    @RequestMapping("/findAll")
   public List<Student> findAll(){

       return studentService.findAll();
    }
}
