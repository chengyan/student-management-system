package com.chy.student.mapper;


import com.chy.student.domain.Score;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface GradeMapper {


    List<Score> findAllCourseGrade();
}
