package com.chy.student.service;

import com.chy.student.domain.Course;
import com.chy.student.mapper.CourseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseService {

    @Autowired
    CourseMapper courseMapper;

    public List<Course> findAllCourse() {
        return courseMapper.findAllCourse();
    }
}
