package com.chy.student.service;


import com.chy.student.domain.Teacher;
import com.chy.student.mapper.TeacherMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherService {

    @Autowired
    TeacherMapper teacherMapper;
    public int addTeacher(Teacher teacher) {

    try {
        teacherMapper.addTeacher(teacher);
        return 1;
    }catch (Exception e){

        return 0;
    }

    }

    public List<Teacher> findAllTeacher() {

      return teacherMapper.findAllTeacher();
    }

    public List<Teacher> findTeacherByName(String name) {

        return teacherMapper.findTeacherByName(name);
    }

    public String deleteTeacher(int id) {

        try {
            teacherMapper.deleteTeacher(id);
            return "id:"+id+"的教师数据删除成功！";
        }catch (Exception e){

            return "删除失败!";
        }

    }

    public String updateTeacher(int id,String name) {

        try {
            teacherMapper.updateTeacher(id,name);

            return "id:"+id+"的教师数据更新成功！";
        }catch (Exception e){

            System.out.println(e.getMessage());
            return "更新失败，请重试!";
        }
    }
}
