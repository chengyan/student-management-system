package com.chy.student.controller;

import com.chy.student.domain.Course;
import com.chy.student.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CourseController {

    @Autowired
    CourseService courseService;

    @RequestMapping(value = "/findAllCourse",method = RequestMethod.GET)
    public List<Course> findAllCourse(){

        return courseService.findAllCourse();
    }

}
