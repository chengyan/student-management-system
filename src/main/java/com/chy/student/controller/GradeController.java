package com.chy.student.controller;

import com.chy.student.domain.Score;
import com.chy.student.service.GradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class GradeController {

    @Autowired
    GradeService gradeService;

    @RequestMapping(value = "/findAllCourseGrade",method = RequestMethod.GET)
    public List<Score> findAllCourseGrade(){

      return  gradeService.findAllCourseGrade();
    }

}
